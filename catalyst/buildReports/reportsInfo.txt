This folder contains production reports generated when running the build script.
This enables the user to monitor changes to the build as the application progresses 
and debug code splitting.