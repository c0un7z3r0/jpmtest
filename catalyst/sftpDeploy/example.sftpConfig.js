const path = require('path')
/*
 *   SFTP Configuration
 *
 *   WARNING: This file should NOT be committed to your version control system
 *   with your private credentials included. This file has been added to the
 *   git ignore file to prevent accidents.
 *
 *   Change the variables to match your requirements:
 *
 *   example SSH authentication object. For more auth options look here: https://github.com/mscdex/ssh2#client-methods
 *   auth: {
 *       host: string; hostname or IP address
 *       port?: number; by default is set to 22
 *       username?: string;
 *       password?: string;
 *   };
 *   localFiles?: string | string[]; glob string or array of local files paths (array of glob strings is not supported)
 *   remotePath?: string; path on remote server where files will be copied
 *   preDeploy?: string[]; array of commands to be executed on remote server before files deploy
 *   postDeploy?: string[]; array of commands to be executed on remote server after files deploy
 *   silent?: boolean; disable logging to console, by default is set to false
 *
 */

module.exports = {
  auth: {
    host: '123.123.123.123',
    username: 'YourUser',
    password: 'YourPassword',
  },
  localFiles: `${path.resolve(__dirname, '../../dist')}/**/*.*`, // or array ['./build/file1.js', 'build/file2.js', 'D:/project/build/file3.js']
  remotePath: '/path/to/remote/directory/on/server', // absoloute path to destination folder on remote server
  // preDeploy: ['df -m', 'cd /home/user/app && ls', 'cd /home/user/app && rm file1.js 2> /dev/null'],
  // postDeploy: ['cd /home/user/app && node file1.js']
}
