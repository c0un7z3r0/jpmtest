const simpleSSHDeploy = require('simple-ssh-deploy')
const config = require('./sftpConfig.js')

simpleSSHDeploy(config)
  .then(() => {
    // deploy succeeded
    console.log('Deployment Complete.')
  })
  .catch(error => {
    // deploy failed
    console.log('Deployment Failed.', error)
  })
