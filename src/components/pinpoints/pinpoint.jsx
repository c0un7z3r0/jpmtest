import React, { useState } from 'react'
import styles from './pinpoint.module.scss'

const Pinpoint = ({
  item,
  x,
  y,
  margin,
  width,
  height,
  valuesPercentile,
  isLastItem,
  minimal,
  index,
}) => {
  const [hovered, setHovered] = useState(false)

  const namesArray = item.names.split(',')

  return (
    <g key={'pinpointGroup_' + index}>
      <g>
        <circle
          className={styles.categoryCircle}
          key={`${item.category}_${index}`}
          cx={x(item.category)}
          cy={y(item.totalValue / valuesPercentile)}
          r={Math.sqrt(8)}
        />
        <circle
          className={styles.hoverAreaCircle}
          key={`${item.category}_hoverArea_${index}`}
          cx={x(item.category)}
          cy={y(item.totalValue / valuesPercentile)}
          r={Math.sqrt(500)}
          onMouseOver={() => setHovered(true)}
          onMouseOut={() => setHovered(false)}
        />
      </g>
      {!minimal && hovered ? (
        <g>
          <circle
            className={styles.categoryRing}
            key={`tooltipCircle_${item.category}_${index}`}
            cx={x(item.category)}
            cy={y(item.totalValue / valuesPercentile)}
            r={Math.sqrt(25)}
          />

          <line
            className={styles.horizontalHoverLine}
            x1={margin}
            x2={margin + width - margin * 5}
            y1={y(item.totalValue / valuesPercentile)}
            y2={y(item.totalValue / valuesPercentile)}
          />
          <line
            className={styles.verticalHoverLine}
            x1={x(item.category)}
            x2={x(item.category)}
            y1={margin}
            y2={margin + height - margin * 3}
          />

          <defs>
            <g id="tooltip" transform={isLastItem ? 'translate(-150,-90)' : 'translate(10,-60)'}>
              <rect
                className={styles.tooltipBox}
                x={x(item.category)}
                y={y(item.totalValue / valuesPercentile) - 20}
                rx={5}
                ry={5}
                width="140"
                height={(namesArray.length + 1) * 23}
              />

              <text
                className={styles.namesLabel}
                x={x(item.category)}
                y={y(item.totalValue / valuesPercentile)}
                transform="translate(5, 0)"
              >
                Names:
              </text>
              <text
                className={styles.names}
                x={x(item.category)}
                y={y(item.totalValue / valuesPercentile)}
                transform="translate(5, 0)"
              >
                {namesArray.map((name, index) => (
                  <tspan x={x(item.category)} dy="1.2em" key={index}>
                    {`${name},`}
                  </tspan>
                ))}
              </text>
            </g>
          </defs>
        </g>
      ) : null}
    </g>
  )
}

export default Pinpoint
