import React, { useState } from 'react'
import propTypes from 'prop-types'
import InputRange from './inputRange'
import styles from './dualInputRange.module.scss'

// Enhance with:
//    invalid ranges eg. 1 - 10
//      if 6-10 are invalid, this should be communicated
//      in the ui visuals slidebar in some way.

const DualInputRange = ({ id, min, max, step, onChange, onFocus, onBlur }) => {
  const [maxInputValue, setMaxInputValue] = useState(max)
  const [minInputValue, setMinInputValue] = useState(min)

  const moveMaxInputValue = value => {
    if (value <= minInputValue) {
      setMaxInputValue(Number(minInputValue))
    } else {
      setMaxInputValue(Number(value))
    }
  }
  const moveMinInputValue = value => {
    if (value >= maxInputValue) {
      setMinInputValue(Number(maxInputValue))
    } else {
      setMinInputValue(Number(value))
    }
  }

  onChange([minInputValue, maxInputValue])

  return (
    <div className={styles.inputRangesContainer}>
      <input
        type="range"
        className={styles.maxInputRange}
        min={min}
        max={max}
        step={step}
        value={maxInputValue}
        onChange={e => moveMaxInputValue(e.target.value)}
      />
      <input
        type="range"
        className={styles.minInputRange}
        min={min}
        max={max}
        step={step}
        value={minInputValue}
        onChange={e => moveMinInputValue(e.target.value)}
      />
    </div>
  )
}

export default DualInputRange
