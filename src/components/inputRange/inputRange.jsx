import React, { useState } from 'react'
import propTypes from 'prop-types'

import styles from './inputRange.module.scss'

// Enhance with:
//    invalid ranges eg. 1 - 10
//      if 6-10 are invalid, this should be communicated
//      in the ui visuals slidebar in some way.

const InputRange = ({ className, id, min, max, step, value, onChange, onFocus, onBlur }) => {
  const [percent, setPercent] = useState(100)

  onChange(percent)

  return (
    <input
      type="range"
      className={styles.inputRange + ' ' + className}
      id={id}
      min={min}
      max={max}
      step={step}
      value={percent}
      onChange={e => setPercent(e.target.value)}
      onFocus={onFocus}
      onBlur={onBlur}
    />
  )
}

InputRange.propTypes = {
  id: propTypes.string,
  min: propTypes.number,
  max: propTypes.number,
  step: propTypes.number,
  value: propTypes.number,
  onChange: propTypes.func,
  onFocus: propTypes.func,
  onBlur: propTypes.func,
}

export default InputRange
