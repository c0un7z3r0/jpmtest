import React from 'react'
import styles from './loadingSpinner.module.scss'

const LoadingSpinner = () => 
<div className={styles.container}>
  <div className={styles.spinner}></div>
</div>

export default LoadingSpinner
