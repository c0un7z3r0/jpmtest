import React from 'react'
import * as d3 from 'd3'
import styles from './graph.module.scss'

import Pinpoint from './pinpoints/pinpoint'
import { min } from 'd3'

const Graph = ({ data, height, width, margin, minimal, minRange, maxRange, yPercent }) => {
  const h = minimal ? 50 : height - 2 * margin
  // const w = width - 2 * margin
  const w = width - 4 * margin

  data.sort((a, b) => (a.category > b.category ? 1 : b.category > a.category ? -1 : 0))

  //Calculate Percentage Operator
  let valuesTotal = 0
  for (var i = 0; i < data.length; i++) {
    valuesTotal += data[i].value
  }
  let valuesPercentile = valuesTotal / 100

  //number formatter
  const xFormat = d3.format('1')

  // Create A Graph Array that adds up the values by category
  var holder = {}
  data.forEach(d => {
    if (holder.hasOwnProperty(d.category)) {
      holder[d.category] = {
        category: d.category,
        totalValue: holder[d.category].totalValue + d.value,
        names: holder[d.category].names + ',' + d.user,
      }
    } else {
      holder[d.category] = {
        category: d.category,
        totalValue: d.value,
        names: d.user,
      }
    }
  })
  var mungedData = []
  for (var prop in holder) {
    if (holder[prop].category >= minRange && holder[prop].category <= maxRange) {
      mungedData.push({
        category: holder[prop].category,
        totalValue: holder[prop].totalValue,
        names: holder[prop].names,
      })
    }
  }

  //x scale
  const x = d3
    .scaleLinear()
    .domain(d3.extent(mungedData, d => d.category)) //domain: [min,max] of a
    .range([margin, w])

  //y scale
  const y = d3
    .scaleLinear()
    .domain([0, minimal ? 20 : yPercent]) // domain [0,max] of b (start from 0)
    .range([h, margin])

  //Line Drawing Function
  const line = d3
    .line()
    .x(d => x(d.category))
    .y(d => y(d.totalValue / valuesPercentile))

  //Category Axis
  const xTicks = x.ticks(maxRange - minRange).map(d =>
    x(d) >= margin && x(d) <= w && d >= minRange && d <= maxRange ? (
      <g transform={`translate(${x(d)},${h + margin - 5})`} key={'xTicks' + d}>
        <text className={styles.axisLabels} transform={`translate(-4, -8)`}>
          {xFormat(d)}
        </text>
        <line
          className={styles.axisTicks}
          x1="0"
          x1="0"
          y1="0"
          y2="5"
          transform="translate(0,-25)"
        />
      </g>
    ) : null
  )

  //Percent Value Axis
  const yTicks = y.ticks(10).map(d =>
    y(d) > 10 && y(d) < h ? (
      <g transform={`translate(${margin - 15},${y(d)})`} key={'yTicks' + d}>
        <text className={styles.axisLabels} x="5" y="5" textAnchor="end">
          {xFormat(d)}
        </text>
        <line
          className={styles.axisTicks}
          x1="0"
          x1="5"
          y1="0"
          y2="0"
          transform="translate(10,0)"
        />
        <line
          className={styles.axisGridline}
          x1="0"
          x2={w - margin}
          y1="0"
          y2="0"
          transform={`translate(15,0)`}
        />
      </g>
    ) : null
  )

  return (
    <svg
      width={width - margin * 2}
      height={height}
      className={minimal ? styles.minimalGraph : styles.mainGraph}
    >
      {/* <rect x={0} y={10} width={width - margin * 2} height={height - margin} fill={'white'} /> */}

      <g transform={`translate(40, ${minimal ? '-20' : '0'})`}>
        {minimal ? null : (
          <React.Fragment>
            <text
              className={styles.verticalAxisLabel}
              x={-20}
              y={height / 2}
              textAnchor="start"
              transform={`rotate(-90,0,${height / 2})`}
            >
              Value Percent
            </text>
            <text
              className={styles.horizontalAxisLabel}
              x={width / 2 - margin * 2 - 30}
              y={height - margin}
            >
              Category
            </text>
            <line className={styles.axis} x1={margin} x2={w} y1={h} y2={h} key="horizontalAxis" />
            <line
              className={styles.axis}
              x1={margin}
              x2={margin}
              y1={margin}
              y2={h}
              key="verticalAxis"
            />
          </React.Fragment>
        )}

        <path className={styles.chartPath} d={line(mungedData)} key="linepath" />

        {minimal ? null : (
          <React.Fragment>
            <g key={'axisLabel1'}>{xTicks}</g>
            <g key={'axisLabel2'}>{yTicks}</g>
          </React.Fragment>
        )}

        {mungedData.map((item, index) => (
          <Pinpoint
            item={item}
            x={x}
            y={y}
            margin={margin}
            width={width}
            height={height}
            valuesPercentile={valuesPercentile}
            index={index}
            isLastItem={index == mungedData.length - 1}
            minimal={minimal}
            key={item.category}
          />
        ))}
        {minimal ? null : <use xlinkHref="#tooltip" />}
      </g>
    </svg>
  )
}

export default Graph
