import React, { useRef, useState } from 'react'
import Graph from './components/graph'
import LoadingSpinner from './components/loadingSpinner/loadingSpinner'

import importedData from './data.json'
import useDimensions from './hooks/useDimensions'

import './public/css/normalize.css'
import './public/css/typography.scss'
import './theme/global.scss'

import styles from './app.module.scss'
import DualInputRange from './components/inputRange/dualInputRange'
import InputRange from './components/inputRange/InputRange'

const App = () => {
  const ref = useRef()
  const size = useDimensions(ref)
  const [minRange, setMinRange] = useState(5)
  const [maxRange, setMaxRange] = useState(14)
  const [percentRange, setPercentRange] = useState(100)

  const sliderRangeChange = values => {
    setMinRange(values[0])
    setMaxRange(values[1])
  }
  const percentRangeChange = value => {
    setPercentRange(value)
  }

  return (
    <main className={styles.container}>
      <div className={styles.header}>
        <h2>JP Morgan Test</h2>
        <h4>
          <strong>D3.js Graph Example</strong>
        </h4>
      </div>
      <div className={styles.application} ref={ref}>
        {size.height == 0 ? (
          <LoadingSpinner />
        ) : (
          <React.Fragment>
            <div className={styles.mainGraph}>
              <Graph
                data={importedData}
                width={size.width}
                height={(size.height / 4) * 3}
                margin={30}
                minimal={false}
                minRange={minRange}
                maxRange={maxRange}
                yPercent={percentRange}
              />
              <div className={styles.verticalRangeInputsContainer}>
                <InputRange
                  min={0}
                  max={100}
                  step={5}
                  onChange={percent => percentRangeChange(percent)}
                />
              </div>
            </div>
            <div className={styles.smallGraph}>
              <Graph
                data={importedData}
                width={size.width}
                height={60}
                margin={30}
                minimal={true}
                minRange={5}
                maxRange={14}
                yPercent={null}
              />
            </div>
            <div className={styles.horizontalRangeInputsContainer}>
              <DualInputRange
                min={5}
                max={14}
                step={1}
                onChange={values => sliderRangeChange(values)}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    </main>
  )
}

export default App
