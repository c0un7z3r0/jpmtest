// WEB BUILD Entry point
// Used for both 'dev' mode and 'production' mode builds
import React from 'react'
import { render } from 'react-dom'
import App from './app'

render(<App />, document.getElementById('application'))
